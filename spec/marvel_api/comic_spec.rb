require 'spec_helper'
require 'byebug'

RSpec.describe 'comic' do
  before (:all) do
    @client =
      MarvelApi.new(marvel_api_key: ENV['MARVEL_KEY'],
                    marvel_api_secret: ENV['MARVEL_SECRET'])
  end

  describe '#comics' do
    before(:all) do
      VCR.use_cassette('comics', match_requests_on: [:method, VCR.request_matchers.uri_without_params(:hash, :ts)]) do
        @comics = @client.comics
      end
    end

    it 'returns a hash of results' do
      expect(@comics).to be_a(Hash)
    end

    it 'gets valid results returned' do
      expect(@comics['results'].count).to eq(20)
    end
  end

  describe '#comic' do
    before(:all) do
      VCR.use_cassette('comic41531', match_requests_on: [:method, VCR.request_matchers.uri_without_params(:hash, :ts)]) do
        @comics = @client.comic(41_531)
      end
    end

    context 'valid calls' do
      it 'returns a hash of results' do
        expect(@comics).to be_a(Hash)
      end

      it 'gets valid results returned' do
        expect(@comics['results'].count).to eq(1)
      end
    end
  end

  describe '#comic_characters' do
    before(:all) do
      VCR.use_cassette('comic41531characters', match_requests_on: [:method, VCR.request_matchers.uri_without_params(:hash, :ts)]) do
        @comics = @client.comic_characters(41_531)
      end
    end

    context 'valid calls' do
      it 'returns a hash of results' do
        expect(@comics).to be_a(Hash)
      end

      it 'gets valid results returned' do
        expect(@comics['results'].count).to eq(2)
      end
    end
  end

end
