require 'spec_helper'

RSpec.describe MarvelApi::Client do

  describe '#initialize' do
    it 'sets the api_key and api_secret from the hash' do
      params = { marvel_api_key: 'apik', marvel_api_secret: 'apis' }
      @client = MarvelApi::Client.new(params)

      expect(@client.api_key).to eq(params[:marvel_api_key])
      expect(@client.api_secret).to eq(params[:marvel_api_secret])
    end
  end

  describe '#method_missing' do
    before (:each) do
      params = { marvel_api_key: 'apik', marvel_api_secret: 'apis' }
      @client = MarvelApi::Client.new(params)
    end

    context 'for general functions' do
      it 'includes params in the connection.get call for single methods' do
        params = { nameStartsWith: 'Hank Pym' }
        expect_any_instance_of(MarvelApi::Connection).to receive(:get).with('comics/1', params)
        @client.comic(1, params)
      end

      it 'includes params in the connection.get call for index methods' do
        params = { nameStartsWith: 'Hank Pym' }
        expect_any_instance_of(MarvelApi::Connection).to receive(:get).with('comics', params)
        @client.comics(params)
      end

      it 'includes params in the connection.get call for combo methods' do
        params = { nameStartsWith: 'Hank Pym' }
        expect_any_instance_of(MarvelApi::Connection).to receive(:get).with('comics/1/characters', params)
        @client.comic_characters(1, params)
      end
    end

    context 'comic functions' do
      it 'calls get on comic for single method' do
        expect_any_instance_of(MarvelApi::Connection).to receive(:get).with('comics/1')
        @client.comic(1)
      end

      it 'calls get on comics for an index method' do
        expect_any_instance_of(MarvelApi::Connection).to receive(:get).with('comics')
        @client.comics
      end

      it 'calls get on comic_characters for a combo method' do
        expect_any_instance_of(MarvelApi::Connection).to receive(:get).with('comics/1/characters')
        @client.comic_characters(1)
      end
    end
  end

end
