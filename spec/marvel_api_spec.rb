require 'spec_helper'

RSpec.describe MarvelApi do
  it 'has a version number' do
    expect(MarvelApi::VERSION).not_to be nil
  end

  it 'instantiates a Marvel::Client on a new call' do
    expect(MarvelApi.new).to be_a(MarvelApi::Client)
  end
end
