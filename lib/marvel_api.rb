require 'marvel_api/version'
require_relative 'marvel_api/client'

# Api wrapper for interacting with the Marvel API
module MarvelApi
  class << self
    # Alias for MarvelApi::Client.new
    # @return [Marvel::Client]
    def new(params = {})
      @client ||= MarvelApi::Client.new(params)
    end
  end
end
