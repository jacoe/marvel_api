require_relative 'connection'
require 'active_support/inflector'

module MarvelApi
  # Client class for the Marvel Api
  class Client
    include MarvelApi::Connection

    VALID_ENTITIES = %i(comic character creator event serie story).freeze

    attr_accessor :api_key, :api_secret

    def initialize(params = {})
      @api_key    = params.fetch(:marvel_api_key,    nil)
      @api_secret = params.fetch(:marvel_api_secret, nil)
    end

    def method_missing(method_name, *args, &block)
      if single_entity_method? method_name
        call_method = "#{method_name.to_s.pluralize}/#{args[0]}"
        args[1].nil? ? get(call_method) : get(call_method, args[1])
      elsif index_entity_method? method_name
        call_method = method_name.to_s
        args[0].nil? ? get(call_method) : get(call_method, args[0])
      elsif combo_entity_method? method_name
        call_method = "#{split_combo_method_to_s(method_name)[0].pluralize}/#{args[0]}/#{split_combo_method_to_s(method_name)[1]}"
        args[1].nil? ? get(call_method) : get(call_method, args[1])
      end
    end

    private

    def single_entity_method?(method_name)
      VALID_ENTITIES.include? method_name
    end

    def index_entity_method?(method_name)
      VALID_ENTITIES.include? method_name.to_s.singularize.to_sym
    end

    def combo_entity_method?(method_name)
      split_combo_method_to_s(method_name).size == 2 \
      && VALID_ENTITIES.include?(split_combo_method_to_sym(method_name)[0]) \
      && VALID_ENTITIES.include?(split_combo_method_to_s(method_name)[1].singularize.to_sym)
    end

    def split_combo_method_to_s(method_name)
      method_name.to_s.split(/_/)
    end

    def split_combo_method_to_sym(method_name)
      method_name.to_s.split(/_/).collect(&:to_sym)
    end

  end
end
