require 'digest/md5'
require 'faraday'
require 'faraday_middleware'
require 'json'
require 'time'
require_relative 'error'

module MarvelApi
  # Module for connecting to the Marvel Api
  module Connection
    BASE_URL = 'http://gateway.marvel.com/v1/public/'.freeze
    HEADERS  = { accept: 'application/json' }.freeze

    def get(path, params = {})
      request(:get, path, params)
    end

    private

    def request(method, path, params)
      etag = params.delete(:etag)
      response = connection.send(method) do |request|
        request.url(path, params.merge(auth))
        request.headers['If-None-Match'] = etag if etag
      end
      prepare(response)
    end

    def prepare(response)
      case response.status
      when 200 then response.body['data']
      else MarvelApi::Error.new(response.body)
      end
    end

    def connection
      options = {
        headers: HEADERS,
        ssl: { verify: false },
        url: BASE_URL
      }

      Faraday.new(options) do |connection|
        connection.use Faraday::Request::UrlEncoded
        connection.use Faraday::Response::ParseJson
        connection.adapter(Faraday.default_adapter)
      end
    end

    def auth(timestamp = current_timestamp)
      { apikey: api_key, ts: timestamp, hash: hash(timestamp) }
    end

    def hash(timestamp)
      Digest::MD5.hexdigest(timestamp + api_secret + api_key)
    end

    def current_timestamp
      Time.now.to_i.to_s
    end
  end
end
