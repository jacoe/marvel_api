module MarvelApi
  # Class for returning Marvel Api specific errors
  class Error < StandardError
    attr_reader :code, :status

    def initialize(response)
      @code = response['code']
      @status = response['status'] || response['message']
    end

    def to_s
      "#{@code} #{@status}"
    end
  end

end
